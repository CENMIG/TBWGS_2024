This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by.png" alt="drawing" width="100"/>

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by.png
