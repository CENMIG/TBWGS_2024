# Basic Linux command

## Lesson outcomes

* Learners can recognise and unmistakably understand the structure of a command.
* Learners understand that the behaviour of many programs can be manipulated by providing arguments.
* Learners can properly construct and successfully execute their own commands to accomplish tasks.
* Learners can seek helps from both local and online sources.
* Learners understand the Linux filesystem and can navigate through the system using `pwd`, `ls`, and `cd`.
* Learners can read contents of text files using `cat`, `less`, `head`, and `tail`.

## Introduction

Computer programs serve as tools to help us doing our tasks. Similar to other good tools, computer programs lack the ability to think creatively (in other words, dumb); they are static and follow instructions religiously. To make use them effectively, it's essential to communicate with them clearly, guiding them toward our goals.  

Numerous sequence analysis programs are exclusively accessible through a command-line interface (CLI). This design choice allows developers to prioritise the development of analytical components, the backbone of a program, over aesthetics. Thus, new algorithms can be swiftly implemented and deployed, accelerating the advancement of the field.  

This lesson covers the fundamentals of a command language interpreter (aka, shell) named `Bash`. `Bash` acts as an interface between you and operating system (OS). Importantly, `Bash` has its own specific syntax (a way it reads and processes commands). In order to make `Bash` follows your commands, you need to first know the`Bash` syntax.

## Command structure

A __command__ is an instruction for a computer to perform a particular task. A command consists of one or more words. A __word__ is defined as a sequence of continuous characters _or_ a sequence of characters and spaces enclosed within quotation marks.

Let's see what is qualified as __words__.

| Number of words | Examples |
|-----------------|----------|
| 1 | abc, apple, 'banana smoothie', "caffeinated coconut juice" |
| 2 | coconut juice, dopamine receptor, extincted "*Panthera tigris sondaica*" |

For example, this command `echo` consists of 1 word, `echo`.  
And this command `echo a b c` consists of 4 words, `echo`, `a`, `b`, and `c`

When we pass a command to `Bash`, the first word of a command specifies the program to be executed. In the case of `echo a b c`, `Bash` will execute a program called `echo`.  
<br>

</br> 

*Note. There are some characters and words that* `Bash` treats them differently, they are not included in this lesson. You can learn more about them by searching `Bash metacharacters` or `Bash reserved words` for more information.

## Having arguments

Now, go to the terminal. You'll typically see something like below.

```
$  
```

This is __prompt__. By default, a terminal shows `$` indicating that`Bash` is ready to accept a new command.

Let's try out our first command.

```
$ echo

$  
```

After passing the command to `Bash` by pressing `Enter`, you'll notice that the prompt disappears momentarily, and a blank line is created. Then, the prompt reappears. The line without `$` indicates the output from the program`echo`. `echo` prints out what we pass to it, which is nothing in this case.

Next, let's enter the second command `echo abc`.

```
$ echo abc  
abc  
$  
```

When you pass extra words to the program `echo`, the terminal prints out those extra words. Words following the program's name are called __arguments__. In this case, the program is `echo`, and the argument is `abc`.
Arguments allow us to modify behaviours of programs. For example, in the case of `echo`,  different arguments can be used to print out different texts.

To learn more about behaviours and functionalities of `echo`, add a word `--help` to the command. It will open the program's help pages, providing the program's syntax, describes the behaviours of the program, and lists __options__ (specific arguments that are predefined) of the program.

```
$ echo --help
Usage: echo [SHORT-OPTION]... [STRING]...
  or:  echo LONG-OPTION
Echo the STRING(s) to standard output.

  -n             do not output the trailing newline
  -e             enable interpretation of backslash escapes
  -E             disable interpretation of backslash escapes (default)
      --help     display this help and exit
      --version  output version information and exit

If -e is in effect, the following sequences are recognized:

  \\      backslash
  \a      alert (BEL)
  \b      backspace
  \c      produce no further output
  \e      escape
  \f      form feed
  \n      new line
  \r      carriage return
  \t      horizontal tab
  \v      vertical tab
  \0NNN   byte with octal value NNN (1 to 3 digits)
  \xHH    byte with hexadecimal value HH (1 to 2 digits)

NOTE: your shell may have its own version of echo, which usually supersedes
the version described here.  Please refer to your shell's documentation
for details about the options it supports.

GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
Full documentation at: <https://www.gnu.org/software/coreutils/echo>
or available locally via: info '(coreutils) echo invocation'
$
```

You can open the help page of many programs this way. Note that not all programs are created equally; you may need to provide different arguments to do the same thing. Typical arguments to open a help page are `--help`, `-h`, and `help`.

## You are not alone

`Bash` is easy to learn, but challenging to master. As you are learning new things, remember that making mistakes and getting into troubles are part of the learning process. You're not alone. Many have been in your shoes and overcome the obstacles.

Since `Bash` is very popular and has been in the spotlight for a long time, there are plenty of resources generated by the community you can use along this learning journey. For example, a build-in help page is a very good starting point to learn about a program. The internet and online communities also provides lots of useful information and support.

Let's try to get some help. Execute a text editor program called `vim`.

```
$ vim
```

You'll be redirected to a new page.

```
~                                                      
~                  VIM - Vi IMproved                   
~                                                      
~                  version 8.1.1847                    
~              by Bram Moolenaar et al.                
~       Modified by team+vim@tracker.debian.org        
~     Vim is open source and freely distributable      
~                                                      
~            Help poor children in Uganda!             
~   type  :help iccf<Enter>       for information      
~                                                      
~   type  :q<Enter>               to exit              
~   type  :help<Enter>  or  <F1>  for on-line help     
~   type  :help version8<Enter>   for version info     
~                                                                                                      
```

__Here's your task__: find a way, either from local or online, to exit `vim` and return to the prompt.

## Filesystem structure

Similar to Microsoft Windows and macOS, the filesystem of Linux-based OS uses a multi-level hierarchy structure. This mean a __directory__ (as famously known as a folder) can contain other directories, which can further contains other directories, and so on. You can also consider this structure as a nested organisation. Let's have a look.
Execute a command `pwd` in your terminal.
```
$ pwd
/home/kittisak
$
```
`pwd` is a command to output __path__ of the present working directory (pwd). The output `/home/kittisak` contain 3 parts. 
1. `/`: the `root` directory, denoted by a forward slash. `root` is the top-level directory in the filesystem. Every directory fundamentally stems from `root`. 
2. `home`: directory located within the `root` directory. In many Linux-based OS, `home` contains user-specific directories.
3. `kittisak`: directory within `home`. This one is a user's specific directory, in this case Kittisak's specific directory.

 Examine your output. Do you spot any differences? Does your output path contain 3 parts as in the example?
 Understand this structure help you to successfully navigate across the filesystem.

## Filesystem navigation

One of the most important commands in CLI is the command to move from one directory to another, `cd`, short-handed for changing directory. `cd` is equivalent to double clicking an icon of a directory on Windows Explorer.
```
$ pwd
/home/kittisak
$ cd Documents
$ pwd
/home/kittisak/Documents
```
When `cd` is executed, it does not create any output. But by using `pwd`, you can confirm that the pwd has changed from `/home/kittisak` to `/home/kittisak/Documents`. 

`cd` accepts two forms of destination: an absolute path or a relative path.
- Absolute path: a path that start with a forward slash `/`. It specifies the complete path of a directory in the filesystem hierarchy.
- Relative path: a path that does not start with a forward slash. This form indicates that the path is relative to the pwd.

For example, let's say `kittisak` contains 2 subdirectories, `Document` and `Downloads`.
```
kittisak/
├── Documents
└── Downloads
```
While being in `/home/kittisak/Documents`, pwd can be changed to `/home/kittisak/Downloads` using an absolute path `/home/kittisak/Downloads`.
```
$ pwd
/home/kittisak/Documents
$ cd /home/kittisak/Downloads
$ pwd
/home/kittisak/Downloads
```

But note that simply using a relative path `Downloads` would not work.
```
$ pwd
/home/kittisak/Documents
$ cd Downloads
cd: Downloads: No such file or directory
$ pwd
/home/kittisak/Documents
```
`cd` raised an error, `Downloads: No such file or directory`. This is simply because `Downloads` is contained within `kittisak` directory, not `Documents`.

Imagine Kittisak needs to work in a very deep directory.
```
/home/kittisak/documents/work/projects/2024/march/day6/mtb/yala/1987/sequencing
```

It would be very tedious to type the whole absolute path every time he wants to move to another deep directory, for example: 
```
/home/kittisak/documents/work/projects/2024/march/day6/mtb/bangkok/2000-2018/sequencing/sample_099
```

`cd` offers a special word `..` for navigating up one level.
```
$ pwd
/home/kittisak/documents/work/projects/2024/march/day6/mtb/yala/1987/sequencing
$ cd ..
$ pwd
/home/kittisak/documents/work/projects/2024/march/day6/mtb/yala/1987
$ cd ../../..
$ pwd
/home/kittisak/documents/work/projects/2024/march/day6
```
with `../../..` (3 times of `..`), the pwd moves up 3 levels of directories.

___With `..`, we can thereby create a relative path that traverses up the filesystem, allowing us move to another adjacent branch of directories without spelling out an absolute path all the times.___
```
$ pwd
/home/kittisak/documents/work/projects/2024/march/day6/mtb/yala/1987/sequencing
$ cd ../../../bangkok/2000-2018/sequencing/sample_099
$ pwd
/home/kittisak/documents/work/projects/2024/march/day6/mtb/bangkok/2000-2018/sequencing/sample_099
```

## When in doubt, `ls`

Sometimes, we want to 
```
$ cd output
```
But `cd` says
```
cd: output: No such file or directory
```
As a computer program cannot think, it never lies. This means the `output` directory does not exist in the pwd.
To confirm this, use the command `ls` to output a list of files and directories in the pwd.
```
$ ls
Documents
Downloads
```
`ls` can show more information by using arguments. Try these commands:

- `ls -a`
- `ls -l`
- `ls -a -l`

To get more information about `ls`, consult its help page using `ls --help` command.

## Read text files

There are several ways to read a text file. But we need to have a text file first. Download a text file from the EFF server using the `curl` command.
```
curl -o eff.txt https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt
```
The program `curl` transfers the data from `https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt` to your computer. Then, as directed by the argument `-o`, the program saves the data into a file named `eff.txt`. The file `eff.txt` contains 7,776 lines of texts.

### `cat`

`cat` outputs everything from a file to the terminal, all at once.
```
$ cat eff.txt
...
...
...
66661   zone
66662   zoning
66663   zookeeper
66664   zoologist
66665   zoology
66666   zoom
```
`cat` is useful when you want to print out the entire content to the terminal.

### `less`

Unlike `cat`, `less` is more user-friendly to use. It has its own screen for viewing text files. You can navigate the whole text file using `↑`, `↓`, `←`, `→`,  `Space bar`, `Page Up`, `Page Down`, and other shortcuts.
```
$ less eff.txt
```
```
11111   abacus
11112   abdomen
11113   abdominal
11114   abide
11115   abiding
11116   ability
11121   ablaze
11122   able
11123   abnormal
11124   abrasion
11125   abrasive
11126   abreast
:
```
Press `q` to quit and return to the prompt.

### `head` and `tail`

To get a preview of a text file, you can use `head` and `tail`. `head` outputs the first few lines, while `tail` outputs the last few lines.
```
$ head eff.txt
11111   abacus
11112   abdomen
11113   abdominal
11114   abide
11115   abiding
11116   ability
11121   ablaze
11122   able
11123   abnormal
11124   abrasion
```
```
$ tail eff.txt
66653   zips
66654   zit
66655   zodiac
66656   zombie
66661   zone
66662   zoning
66663   zookeeper
66664   zoologist
66665   zoology
66666   zoom
```
You can print out more lines by adding the argument `-n` followed by an integer. This trick works with both `head` and `tail`.
```
$ head -n 24 eff.txt
11111   abacus
11112   abdomen
11113   abdominal
11114   abide
11115   abiding
11116   ability
11121   ablaze
11122   able
11123   abnormal
11124   abrasion
11125   abrasive
11126   abreast
11131   abridge
11132   abroad
11133   abruptly
11134   absence
11135   absentee
11136   absently
11141   absinthe
11142   absolute
11143   absolve
11144   abstain
11145   abstract
11146   absurd
```

---
We hope you now know how to use command line programs and interact with your computer through `Bash`. This lesson is only just an introductory lesson, the very first step in molecular sequence analysis. There are many more programs available only with CLI in this world. Keep learning and don't be afraid. In the next lesson, you will learn how to use command line programs for read quality assessment and control. 
